using System.Text;

namespace APORTE
{
    public class figura
    {
        public string nombrefigura;
        public string color;


        public figura(string nombrefigura, string color)
        {
            this.nombrefigura = nombrefigura;
            this.color = color;

        }
        public virtual void MostrarDatos()
        {
            Console.WriteLine("\nNombre de Figura: " + nombrefigura+ "\nColor: " + color);
        }
    }
}