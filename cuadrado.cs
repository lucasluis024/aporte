using System.Text;

namespace APORTE
{
    public class cuadrado : figura, ifigura
    {
        private int resultado { set; get; }

        private int lados { set; get; }
         
        public cuadrado(string nombrefigura, string color, int lados, int resultado) :base(nombrefigura,color)
        {
            this.lados = lados;
        }
          

        public void areacalcular ()
        {
            resultado = (lados +  lados + lados + lados );
        }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Area de un Cuadrado es : " +lados,  "Lado uno" + lados, "Lados dos" + lados, "Lado tres" + lados, "Lado cuatro"  + resultado);
        }
    } 
}