 using System.Text;

namespace APORTE
{
    public class rectangulo : figura
    {
        private int Base;
        private int Altura;
        private int resultado;

        public rectangulo(string nombrefigura, string color, int Base, int Altura): base (nombrefigura,color)
        {
            this.Base = Base;   
            this.Altura = Altura;
        }
        public void areacalcular()
        {
            resultado = Altura *Base ;
        }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Base: " + Base + "\nAltura: " + Altura + "\nEl area de un Rectangulo es: " + resultado);
        }
    }
}