﻿namespace APORTE
{
    internal class Program
    {
        static void Main(string[] args)
        {

            rectangulo rectan = new rectangulo("Rectangulo", "Azul", 30, 30);
            rectan.areacalcular();

            cuadrado cuadra = new cuadrado("Cuadrado", "Blanco", 20, 20);
            cuadra.areacalcular();

            circulo circul = new circulo("Circulo", "Negro", 25);
            circul.areacalcular();

            List<figura> ListaFiguras = new List<figura>();
            {
                ListaFiguras.Add(circul);
                ListaFiguras.Add (cuadra);
                ListaFiguras.Add(rectan);

            }
             foreach (figura Figura in ListaFiguras)
            {
                Figura.MostrarDatos();
            }

        }
    }
}